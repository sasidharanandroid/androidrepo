package com.training.roomassignment.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName="Products")
data class Product(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "product_name") val productname: String,
    @ColumnInfo(name = "product_price") var price: Int,
    @ColumnInfo(name = "product_desc") val desc: String,
    @ColumnInfo(name = "product_stock") val stock: Int
)

//@Entity(tableName="Orders")
//data class Order(
//    @PrimaryKey(autoGenerate = true) val orderid: Int,
//    @ColumnInfo(name = "ordered_product_name") val orderedProductName: String,
//    @ColumnInfo(name = "ordered_product_price") var orderedProductPrice: Int,
//    @ColumnInfo(name = "ordered_product_quantity") val orderedProductQuantity: Int,
//    @ColumnInfo(name = "ordered_product_stock") val orderedProductStock: Int
//)

@Entity(tableName="Customers")
data class Customer(
    @PrimaryKey(autoGenerate = true) val customerid: Int,
    @ColumnInfo(name = "customer_name") val cusotmername: String,
    @ColumnInfo(name = "customer_mobile") var mobilenumber : Int,
    @ColumnInfo(name = "customer_address") val address: String,
    @ColumnInfo(name = "customer_pincode") val pincode: Int
)

@Entity(tableName="Orders")
data class Order(
    @PrimaryKey(autoGenerate = true) val orderid: Int,
    @ColumnInfo(name = "ordered_product_name") val orderedProductName: String,
    @ColumnInfo(name = "ordered_product_price") var orderedProductPrice: Int,
    @ColumnInfo(name = "ordered_product_quantity") val orderedProductQuantity: Int,
    @ColumnInfo(name = "ordered_product_stock") val orderedProductStock: Int
)