package com.training.roomassignment.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface SalesDAO {

    // Get All Products
    @Query("SELECT * FROM Products ORDER BY id ASC")
    fun getAllProducts(): Flow<List<Product>>

    // Get All Customers
    @Query("SELECT * FROM Customers ORDER BY customerid ASC")
    fun getAllCustomers(): Flow<List<Customer>>

    // Get All Orders
    @Query("SELECT * FROM Orders ORDER BY orderid ASC")
    fun getAllOrders(): Flow<List<Order>>

     // Insert Product
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertproduct(product: Product)

    // Insert Customer
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertcustomer(customer: Customer)

    // Insert Order
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertorder(order: Order)

    // Delete Products
    @Query("DELETE FROM products")
    fun deleteAllProduct()

    // Delete Orders
    @Query("DELETE FROM orders")
    fun deleteAllOrders()

    // Delete Customers
    @Query("DELETE FROM customers")
    fun deleteSinglecustomers()
}