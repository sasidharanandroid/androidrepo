package com.training.roomassignment.data

import androidx.annotation.WorkerThread
import kotlinx.coroutines.flow.Flow

class SalesRepository(private val salesDAO: SalesDAO) {

    val allProducts: Flow<List<Product>> = salesDAO.getAllProducts()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(product: Product) {
        salesDAO.insertproduct(product)
    }
}