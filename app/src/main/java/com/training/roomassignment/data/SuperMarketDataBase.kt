package com.training.roomassignment.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = arrayOf(Product::class, Customer::class, Order::class), version = 1, exportSchema = false)

abstract class SuperMarketDataBase : RoomDatabase() {

    abstract fun salesDao(): SalesDAO

    private class SuperMarketDataBaseCallback(private val scope: CoroutineScope) :
        RoomDatabase.Callback() {
        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let { database ->
                scope.launch {
                    val salesDao = database.salesDao()
                    salesDao.deleteAllProduct()

                    var product = Product(0, "Android", 10000, "Android Mobile",20)
                    salesDao.insertproduct(product)

                    var product2 = Product(1, "iPhone", 25000, "IOS Mobile",30)
                    salesDao.insertproduct(product2)

                    var product3 = Product(2, "iPhone2", 25000, "IOS Mobile",18)
                    salesDao.insertproduct(product3)

                    var product4 = Product(3, "iPhone3", 25000, "IOS Mobile",5)
                    salesDao.insertproduct(product4)

                }
            }
        }
    }

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: SuperMarketDataBase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): SuperMarketDataBase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    SuperMarketDataBase::class.java,
                    "product_database"
                )
                    .addCallback(SuperMarketDataBaseCallback(scope))
                    .build()
                INSTANCE = instance
                instance
            }
        }
    }
}