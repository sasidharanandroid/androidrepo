package com.training.roomassignment.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.training.roomassignment.data.Product
import com.training.roomassignment.data.SalesRepository

class SalesVIewModel(private val repository: SalesRepository) : ViewModel() {

    val allProduct: LiveData<List<Product>> = repository.allProducts.asLiveData()
}